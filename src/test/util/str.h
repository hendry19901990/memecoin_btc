// Copyright (c) 2019 The Bitcoin Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef MEMECOIN_TEST_UTIL_STR_H
#define MEMECOIN_TEST_UTIL_STR_H

#include <string>

bool CaseInsensitiveEqual(const std::string& s1, const std::string& s2);

#endif // MEMECOIN_TEST_UTIL_STR_H
