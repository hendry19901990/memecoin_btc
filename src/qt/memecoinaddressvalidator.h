// Copyright (c) 2011-2014 The Memecoin Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef MEMECOIN_QT_MEMECOINADDRESSVALIDATOR_H
#define MEMECOIN_QT_MEMECOINADDRESSVALIDATOR_H

#include <QValidator>

/** Base58 entry widget validator, checks for valid characters and
 * removes some whitespace.
 */
class MemecoinAddressEntryValidator : public QValidator
{
    Q_OBJECT

public:
    explicit MemecoinAddressEntryValidator(QObject *parent);

    State validate(QString &input, int &pos) const;
};

/** Memecoin address widget validator, checks for a valid memecoin address.
 */
class MemecoinAddressCheckValidator : public QValidator
{
    Q_OBJECT

public:
    explicit MemecoinAddressCheckValidator(QObject *parent);

    State validate(QString &input, int &pos) const;
};

#endif // MEMECOIN_QT_MEMECOINADDRESSVALIDATOR_H
