Building Memecoin
================

See doc/build-*.md for instructions on building the various
elements of the Memecoin Core reference implementation of Memecoin.
